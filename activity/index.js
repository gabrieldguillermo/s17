
/*

	How you will be evaluated
	1. No errors should be logged in the console.
	2. prompt() is used to gather information.
	3. alert() is used to show information.
	4. All values must be properly logged in the console.
	5. All variables are named appropriately and defines the value it contains.
	6. All functions are named appropriately and follows naming conventions.

 */


 /*	4.  Create a function which is able to prompt the user to provide their full name, age, and location. 
		- use prompt() and store the returned value into function scoped variables within the function. 
		- show an alert to thank the user for their input.
		- display the user's inputs in messages in the console.
 		- invoke the function to display the user’s information in the console.
		- follow the naming conventions for functions.*/

		function userInput(){
		const userName = prompt("What is your name? ");
		const userAge = prompt("How old are you? ");
		const userLocation = prompt("Where do you live?");
		alert('Thank you for your input!');	

			console.log("Full name: " + userName );
			console.log("Age: " + userAge);
			console.log("Location: " + userLocation);
		};

		userInput();




/*5. Create a function which is able to print/display your top 5 favorite bands/musical artists. 
		- invoke the function to display your information in the console.
		- follow the naming conventions for functions.*/

		function favoriteBands(){
			let myBands=['My Chemical Romance','All American Reject','Fall Out Boys','Bamboo','Kamikazee'];
			console.log("My top 5 favorite bands:");
			let numbering= 1;	
			myBands.forEach(function(band){
				console.log(`${numbering++} ${band}`);
			});	
		};

		favoriteBands();





/*6. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating. 
		- look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        - invoke the function to display your information in the console.
		- follow the naming conventions for functions.*/

		function favoriteMovies(){
		const myMovies=[
			{Title:'Water World',Rating:'2.5/5'},
			{Title:'John Wick',Rating:'86%'},
			{Title: 'Avatar',Rating: '82%'},
			{Title:'Taken', Rating: '59% '},
			{Title: 'The A-Team',Rating: '49%'}
		];

		console.log("My top 5 favorite Movies:");
		console.log(myMovies);
		
		};

		favoriteMovies();




	/*	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.*/

		let printFriends = function printUsers(){
			alert("Hi! Please add the names of your friends.");
			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:");
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};

		printFriends();

		// console.log(friend1);
		// console.log(friend2);
	